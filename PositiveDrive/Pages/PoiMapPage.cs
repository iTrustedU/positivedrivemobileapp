﻿using System.Threading.Tasks;
using Plugin.Geolocator;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Linq;
using System.Collections.Specialized;
using Microsoft.WindowsAzure.MobileServices;
using System;
using PositiveDrive.Models;
using PositiveDrive.Services;
using System.Diagnostics;
using PositiveDrive.Interfaces;
using PositiveDrive.Views;
using PositiveDrive.ViewModels;

namespace PositiveDrive.Pages
{
	public class PoiMapPageBase : ViewModelPage<PoiMapViewModel> { }

	public partial class PoiMapPage : PoiMapPageBase
	{
		public PoiMapPage()
		{
			InitializeComponent();
			ViewModel.Pois.CollectionChanged += ViewModel_CollectionOfPoisChanged;
			ViewModel.OnLocationChanged += ViewModel_OnLocationChanged;
		}

		void ViewModel_OnLocationChanged(object sender, Plugin.Geolocator.Abstractions.Position position)
		{
			mapOfPois.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(position.Latitude, position.Longitude), Distance.FromMiles(1)));
		}

		void ViewModel_CollectionOfPoisChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			if (e.NewItems == null)
				return;

			var newPois = e.NewItems?.OfType<Poi>();

			foreach (var poi in newPois)
			{
				var pin = new Pin
				{
					Type = PinType.Place,
					Position = new Position(poi.Latitude, poi.Longitude),
					Label = FormattedSmilesAmount(poi),
					Address = poi.Description
				};
				mapOfPois.Pins.Add(pin);
			}
		}

		void SavePoi_Clicked(object sender, System.EventArgs e)
		{
			AnimateOutAddNewPoiForm();
		}

		void AddPoi_Clicked(object sender, System.EventArgs e)
		{
			AnimateInAddNewPoiForm();
		}

		async void AnimateInAddNewPoiForm()
		{
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
			buttonAddPoi.TranslateTo(0, buttonAddPoi.Height, 500, Easing.CubicInOut);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
			await layoutAddPoi.TranslateTo(0, (layoutAddPoi.Height + Content.Height) * -0.5F, 500, Easing.CubicInOut);
			entryPoiNumberOfSmilies.Focus();
		}

		void AnimateOutAddNewPoiForm()
		{
			buttonAddPoi.TranslateTo(0, 0, 500, Easing.CubicInOut);
			layoutAddPoi.TranslateTo(0, 0, 500, Easing.CubicInOut);
		}

		static string FormattedSmilesAmount(Poi poi) => $"SMILES: {poi.Smiles}";
	}
}