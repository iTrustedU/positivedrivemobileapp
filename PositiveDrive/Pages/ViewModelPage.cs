﻿using System;
using PositiveDrive.Interfaces;
using Xamarin.Forms;

namespace PositiveDrive.Views
{
	public class ViewModelPage<T> : ContentPage where T : class, IViewModel, new()
	{
		private readonly T _viewModel;

		public T ViewModel => _viewModel;

		protected override void OnAppearing()
		{
			base.OnAppearing();
			ViewModel.Init();
		}

		public ViewModelPage()
		{
			_viewModel = new T();
			BindingContext = _viewModel;
		}
	}
}
