﻿using System;
using Xamarin.Forms;
using PositiveDrive.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.MobileServices.Sync;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using System.Collections.ObjectModel;
using System.Diagnostics;
using PositiveDrive.Interfaces;
using PositiveDrive.Services;

[assembly: Dependency(typeof(AzurePoiService))]
namespace PositiveDrive.Services
{
	// Copied some stuff from azure quickstarter and it works flawlessly. 
	// It even handles deep native errors, like when DNS is unreachable...and other similiar as such.
	public partial class AzurePoiService : IPoiService
	{
		const string offlineDbPath = @"localstore.db";
		private readonly IMobileServiceSyncTable<Poi> poiTable;
		private readonly MobileServiceClient appService;

		public AzurePoiService()
		{
			appService = new MobileServiceClient(Constants.ApplicationURL);
			var store = new MobileServiceSQLiteStore(offlineDbPath);
			store.DefineTable<Poi>();
			appService.SyncContext.InitializeAsync(store, new MobileServiceSyncHandler());
			poiTable = appService.GetSyncTable<Poi>();
		}

		public async Task SavePoiAsync(Poi poi)
		{
			if (poi.Id == null)
			{
				await poiTable.InsertAsync(poi);
			}
			else
			{
				await poiTable.UpdateAsync(poi);
			}
		}

		public async Task SyncOfflineAsync()
		{
			ReadOnlyCollection<MobileServiceTableOperationError> syncErrors = null;

			try
			{
				await appService.SyncContext.PushAsync();
				await poiTable.PullAsync("allStores", poiTable.CreateQuery());
			}
			catch (MobileServicePushFailedException exc)
			{
				if (exc.PushResult != null)
				{
					syncErrors = exc.PushResult.Errors;
				}
			}

			if (syncErrors != null)
			{
				foreach (var error in syncErrors)
				{
					if (error.OperationKind == MobileServiceTableOperationKind.Update && error.Result != null)
					{
						await error.CancelAndUpdateItemAsync(error.Result);
					}
					else
					{
						await error.CancelAndDiscardItemAsync();
					}
					Debug.WriteLine(@"Error executing sync operation. Item: {0} ({1}). Operation discarded.", error.TableName, error.Item["id"]);
				}
			}
		}

		public async Task<IEnumerable<Poi>> GetPoisAsync(bool syncItems = false)
		{
			try
			{
				if (syncItems)
				{
					await SyncOfflineAsync();
				}
				return await poiTable.ToEnumerableAsync();
			}
			catch (Exception e)
			{
				Debug.WriteLine(@"Sync error: {0}", e.Message);
				return await poiTable.ToEnumerableAsync();
			}
		}
	}
}
