﻿using System;
using PositiveDrive.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PositiveDrive.Interfaces
{
	public interface IPoiService
	{
		Task SavePoiAsync(Poi poi);
		Task<IEnumerable<Poi>> GetPoisAsync(bool syncItems = false);
		Task SyncOfflineAsync();
	}
}
