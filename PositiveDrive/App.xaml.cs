﻿using System;
using Xamarin.Forms;
using Plugin.Geolocator.Abstractions;
using PositiveDrive.Pages;

namespace PositiveDrive
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();
			MainPage = new PoiMapPage();
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
