﻿using System;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;

namespace PositiveDrive.Models
{
	public class Poi
	{
		public string Description { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public int Smiles { get; set; }
		public string Id { get; set; }

		[Version]
		public string Version { get; set; }
	}
}
