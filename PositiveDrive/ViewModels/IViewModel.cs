﻿using System;

namespace PositiveDrive.Interfaces
{
	public interface IViewModel
	{
		void Init();
		bool IsBusy { get; }
	}
}
