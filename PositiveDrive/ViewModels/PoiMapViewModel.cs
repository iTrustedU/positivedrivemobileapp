﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using PositiveDrive.Interfaces;
using PositiveDrive.Models;
using PropertyChanged;
using Xamarin.Forms;

namespace PositiveDrive.ViewModels
{
	[ImplementPropertyChanged]
	public class PoiMapViewModel : IViewModel
	{
		public string Description { get; set; }
		public string Smiles { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }

		public readonly ObservableCollection<Poi> Pois;
		public event EventHandler<Position> OnLocationChanged;

		public bool IsBusy
		{
			get;
			protected set;
		}

		private readonly IPoiService poiService;
		private readonly IGeolocator geoLocator;

		public PoiMapViewModel()
		{
			Pois = new ObservableCollection<Poi>();
			poiService = DependencyService.Get<IPoiService>();
			geoLocator = CrossGeolocator.Current;
			geoLocator.DesiredAccuracy = 50;
			geoLocator.PausesLocationUpdatesAutomatically = false;
			geoLocator.PositionChanged += Position_Changed;
		}

		public async void Init()
		{
			if (!geoLocator.IsListening)
				await geoLocator.StartListeningAsync(5, 10, true);
			await GetPoisAsync(true);
		}

		async void Position_Changed(object sender, PositionEventArgs e)
		{
			var position = await geoLocator.GetPositionAsync(10000);
			OnLocationChanged?.Invoke(this, position);
		}

		async Task GetPoisAsync(bool syncItems)
		{
			IsBusy = true;
			var poiList = await poiService.GetPoisAsync(syncItems);
			Pois.Clear();
			foreach (var poi in poiList)
				Pois.Add(poi);
			IsBusy = false;
		}

		public ICommand SavePoi
		{
			get
			{
				return new Command(async (o) =>
				{
					IsBusy = true;

					int smilesInt = 0;
					int.TryParse(Smiles?.Trim(), out smilesInt);

					var poi = new Poi
					{
						Latitude = Latitude,
						Longitude = Longitude,
						Description = Description,
						Smiles = smilesInt,
					};

					await poiService.SavePoiAsync(poi);
					Pois.Add(poi);

					await poiService.SyncOfflineAsync();

					IsBusy = false;
				}, (o) => !string.IsNullOrEmpty(Smiles?.Trim()));
			}
		}

		public ICommand NewPoi
		{
			get
			{
				return new Command(async o =>
				{
					Description = null;
					Smiles = null;
					var position = await geoLocator.GetPositionAsync(10000);
					Latitude = position.Latitude;
					Longitude = position.Longitude;
				});
			}
		}
	}
}
